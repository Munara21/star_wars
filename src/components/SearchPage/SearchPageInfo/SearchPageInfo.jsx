import { Link } from "react-router-dom";
import PropTypes from "prop-types";

import styles from "./SearchPageInfo.module.css";

const SearchPageInfo = ({ people }) => (
 
    <>
      {people.length 
      ? (
        <ul className={styles.list__container}>
          {people.map(({ id, name, img }) => 
            <Link to={`/people/${id}`}>
              <li className={styles.list__item} key={id}>
                <img className={styles.person__img} src={img} alt={name} />
                <p className={styles.person__name}>{name}</p>
              </li>
            </Link>
          )}
        </ul>
      ) : 
        <h2 className={styles.person__comment}>No such character</h2>
      }
    </>
)

SearchPageInfo.propTypes = {
  people: PropTypes.array,
};

export default SearchPageInfo;
