import { useSelector } from "react-redux";
import {  Link } from "react-router-dom";
import icon from './img/bookmark.svg';


import  styles from './Favorite.module.css';
import { useState } from "react";
import { useEffect } from "react";

const Favorite = () => {
    const [count, setCount] = useState();

    const storeData = useSelector(state => state.favoriteReducer);

    useEffect(()=> {
        const length = Object.keys(storeData).length;
        length.toString > 2 ? setCount('...') : setCount(length);
        setCount(length);
    })
     return(
        <div className={styles.container}>
             <Link to="/favorites">
                 <span className={styles.counter}>{count}</span>
                <img className={styles.bookmark} src={icon} alt=" bookmark" />
             </Link>x   

            
        </div>
    )
}

export default Favorite;