import { useEffect, useState } from "react";
import { NavLink } from "react-router-dom";
import {
  useTheme,
  THEME_LIGHT,
  THEME_DARK,
  THEME_NEUTRAL,
} from "@context/ThemeProvider";
import Favorite from "../Favorite";
import styles from "./Header.module.css";

import imgStartLight from "./img/star-war-neutral.svg";
import imgStarColor from "./img/star-war-color2.svg";
import imgVader from "./img/star-war-light.svg";

const Header = () => {
  const [icon, setIcon] = useState(imgStarColor);
  const isTheme = useTheme();

  useEffect(() => {
    switch (isTheme.theme) {
      case THEME_LIGHT:
        setIcon(imgStartLight);
        break;
      case THEME_DARK:
        setIcon(imgStarColor);
        break;
      case THEME_NEUTRAL:
        setIcon(imgVader);
        break;

      default:
        setIcon(imgVader);
        break;
    }
  }, [isTheme]);
  return (
    <div className={styles.container}>
      <img className={styles.logo} src={icon} alt="star wars" />
      <ul className={styles.list__container}>
        <li>
          <NavLink to="/" exact>
            Home
          </NavLink>
        </li>
        <li>
          <NavLink to="/people/?page=1">People</NavLink>
        </li>
        <li>
          <NavLink to="/not-found" exact>
            Not Found
          </NavLink>
        </li>
        <li>
          <NavLink to="/search" exact>
          Search
          </NavLink>
        </li>
        <li>
          <NavLink to="/fail" exact>
          Fail
          </NavLink>
        </li>
      </ul>
      <Favorite />
    </div>
  );
};

export default Header;
