import loaderYellow from './img/loaderYellow.svg';
import  styles from './UiLoading.module.css';

const UiLoading = () => {
     return(
        <img 
            className={styles.loader}
            src={loaderYellow}
            alt="loader"

        />
    )
}

// UiLoading.propTypes = {
//     test: PropTypes.string
// }

export default UiLoading;