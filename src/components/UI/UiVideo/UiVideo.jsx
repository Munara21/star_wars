import PropTypes from "prop-types";
import cn from "classnames";
import styles from "./UiVideo.module.css";

const UiVideo = ({ src, playbackRate = 1.2, classes }) => {
  return (
    <video
      loop
      autoPlay
      className={cn(styles.video, classes)}
      playbackRate={playbackRate}
    >
      <source src={src} />
    </video>
  );
};

UiVideo.propTypes = {
  src: PropTypes.string,
  playbackRate: PropTypes.number,
  classes: PropTypes.string,
};

export default UiVideo;
