import PropTypes from "prop-types";

import { makeConCurrentRequest, changeHttp } from "@utils/network";
import styles from "./PersonFilms.module.css";
import { useState } from "react";
import { useEffect } from "react";

const PersonFilms = ({ personFilms }) => {
  const [filmsName, setFilmsName] = useState([]);

  useEffect(() => {
    (async () => {
      const filmsHttps = personFilms.map((url) => changeHttp(url));
      const response = await makeConCurrentRequest(filmsHttps);

      setFilmsName(response);
    })();
  }, [personFilms]);

  return (
    <div className={styles.wrapper}>
      <ul className={styles.list__container}>
        {filmsName
            .sort((a,z)=> a.episode_id - z.episode_id)
            .map(({ title, episode_id }) => (
           <li className={styles.list__item}key={episode_id}>
            <span className={styles.item__episode}>Episode {episode_id}</span>
            <span className={styles.item__colon}>:</span>
            <span className={styles.item__title}>{title}</span>
          </li>
        ))}
      </ul>
    </div>
  );
};

PersonFilms.propTypes = {
  test: PropTypes.array,
};

export default PersonFilms;
