import PropTypes from "prop-types";
import cn from "classnames";
import {
  useTheme,
  THEME_LIGHT,
  THEME_DARK,
  THEME_NEUTRAL,
} from "@context/ThemeProvider";

import imgLightSide from "./img/light-side.jpeg";
import imgDarkSide from "./img/dark-side.jpeg";
import imgNeutralSide from "./img/neutral-side.jpeg";


import styles from "./ChooseSide.module.css";
const ChooseSideItem = ({ classes, theme, text, img }) => {
  const isTheme = useTheme();
  return (
    <div
      className={cn(styles.item, classes)}
      onClick={() => isTheme.change(theme)}
    >
      <div className={styles.item__header}>{text}</div>
      <img className={styles.item__img} src={img} alt={text} />
    </div>
  );
};
ChooseSideItem.propTypes = {
  classes: PropTypes.string,
  onClick: PropTypes.func,
  text: PropTypes.string,
  img: PropTypes.string,
};

const ChooseSide = () => {
  return (
    <div className={styles.container}>
      <ChooseSideItem
        theme={THEME_LIGHT}
        text="Light Side"
        img={imgLightSide}
        classes={styles.item__light}
      />
      <ChooseSideItem
        theme={THEME_DARK}
        text="Dark Side"
        img={imgDarkSide}
        classes={styles.item__dark}
      />
      <ChooseSideItem
        theme={THEME_NEUTRAL}
        text="Han Solo"
        img={imgNeutralSide}
        classes={styles.item__neutral}
      />
    </div>
  );
};

export default ChooseSide;
