import PeoplePage from '@containers/PeoplePage';
import PersonPage from '@containers/PersonPage';
import HomePage from '@containers/HomePage';
import NotFoundPage from '@containers/NotFoundPage';
import FavoritesPage from '@containers/FavoritesPage/FavoritesPage';
import SearchPage from '@containers/SearchPage/SearchPage';
import ErrorMessage from '@components/ErrorMessage';

const routesConfig = [
    {
        path: '/',
        exact: true,
        component: HomePage

    },
    {
        path: '/people',
        exact: true,
        component: PeoplePage
    },
    {
        path: '/favorites',
        exact: true,
        component: FavoritesPage
    },
    {
        path: '/search',
        exact: true,
        component: SearchPage
    },
    {
        path: '/fail',
        exact: true,
        component: ErrorMessage
    },
    {
        path: '/people/:id',
        exact: true,
        component: PersonPage
    },
    {
        path: '/not-found',
        exact: true,
        component: NotFoundPage 
    },
    {
        path: '*',
        exact: false,
        component: NotFoundPage  
    }
];

export default routesConfig;