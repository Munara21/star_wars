import PropTypes from "prop-types";
import { Link } from "react-router-dom";

import UiButton from "@components/UI/UiButton/UiButton";
import styles from "./PeopleNavigation.module.css";

const PeopleNavigation = ({ getResource, prePage, nextPage, pageCounter }) => {
  const handleChangePrev = () => getResource(prePage);
  const handleChangeNext = () => getResource(nextPage);

  return (
    <div className={styles.container}>
      <Link to={`/people/?page=${pageCounter - 1}`} className={styles.link}>
        <UiButton
          text="Previous"
          onClick={handleChangePrev}
          disabled={!prePage}
         
        />
      </Link>
      <Link to={`/people/?page=${pageCounter + 1}`} className={styles.link}>
        <UiButton 
         text="Next" 
         onClick={handleChangeNext}
          disabled={!nextPage}
        />
      </Link>
    </div>
  );
};

PeopleNavigation.propTypes = {
  getResource: PropTypes.func,
  prePage: PropTypes.string,
  nextPage: PropTypes.string,
  pageCounter: PropTypes.number,
};

export default PeopleNavigation;
