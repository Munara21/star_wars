import { useState, useEffect } from "react";
import PropTypes from 'prop-types';

import { withErrorApi } from "@hoc-helpers/withErrorApi";
import PeopleList from "@components/PeoplePage/PeopleList";
import PeopleNavigation from '@containers/PeoplePage/PeopleNavigation';
import { getApiResource, changeHttp } from "@utils/network";
import { getPeopleId, getPeopleImage, getPeoplePageId } from "@services/getPeopleData";
import { API_PEOPLE } from "@constants/api";
import { useQuery } from "@hooks/useQuery";

const PeoplePage = ({setErrorApi}) => {
  const [people, setPeople] = useState(null);
  const [prePage, setPrePage] = useState(null);
  const [nextPage, setNextPage] = useState(null);
  const [pageCounter, setPageCounter] = useState(1);

  const query  = useQuery();
  const queryPage = query.get('page');

  console.log(prePage, nextPage);

  const getResource = async (url) => {
    const res = await getApiResource(url);

    

    if (res) {
      const peopleList = res.results.map(({ name, url }) => {
        const id = getPeopleId(url);
        const img = getPeopleImage(id);

        return {
          id,
          name,
          img,
        };
      });
      setPeople(peopleList);
      setPrePage(changeHttp(res.previous));
      setNextPage(changeHttp(res.next));
      setPageCounter(getPeoplePageId(url));
      setErrorApi(false);
    } else {
      setErrorApi(true);
    }
  };

  useEffect(() => {
    getResource(API_PEOPLE+queryPage);
    // getResource("https://swapi.dev/api/people/?page=2");
  }, []);

  return (
    <>
     
      <PeopleNavigation 
      getResource={getResource}
      prePage={prePage}
      nextPage={nextPage}
      pageCounter={pageCounter}/>
      

      {people && <PeopleList people={people} />}
    </>
  )
}

PeoplePage.propTypes = {
  setErrorApi: PropTypes.func
}

export default withErrorApi(PeoplePage);

