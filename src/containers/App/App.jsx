import { BrowserRouter, Switch, Route } from "react-router-dom";
import routesConfig from "@routes/routesConfig";
import Header from "@components/Header";

import styles from "./App.module.css";

console.log(styles);
const App = () => {
  return (
    <>
      <BrowserRouter>
        <div className={styles.wrapper}>
          <Header />

          <Switch>
            {routesConfig.map((route, index) => (
              <Route
                key={index}
                path={route.path}
                exact={route.exact}
                component={route.component}
              />
            ))}
          </Switch>
        </div>
        {/* <Route path="/people" exact component={PeoplePage} /> */}
      </BrowserRouter>
    </>
  );
};

export default App;
